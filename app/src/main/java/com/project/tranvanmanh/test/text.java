package com.project.tranvanmanh.test;

/**
 * Created by tranvanmanh on 5/2/2018.
 */

public class text {

    private String name;
    private String address;

    public text(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
